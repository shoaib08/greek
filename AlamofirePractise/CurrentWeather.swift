//
//  CurrentWeather.swift
//  AlamofirePractise
//
//  Created by Vahida Sheikh on 20/07/18.
//  Copyright © 2018 Vahida Sheikh. All rights reserved.
//

import Foundation
class Weather {
    let temperature:Int?
    let humidity:Int?
    let icon:String?
    
    
    struct WeatherKey{
        static let temperature = "temperature"
        static let humidity = "apparentTemperature"
        static let icon = "partly-cloudy-night"
    }
    init(weatherDictionary : [String:Any]) {
        temperature = weatherDictionary[WeatherKey.humidity] as? Int
        if let humidityDouble = weatherDictionary[WeatherKey.humidity] as? Double {
            humidity = Int(humidityDouble * 100)
        } else {
            humidity = nil
        }
        icon = weatherDictionary[WeatherKey.icon] as? String
    }
    

/*
"time": 1532091719,
"summary": "Partly Cloudy",
"icon": "partly-cloudy-night",
"nearestStormDistance": 23,
"nearestStormBearing": 52,
"precipIntensity": 0,
"precipProbability": 0,
"temperature": 57.27,
"apparentTemperature": 57.27,
"dewPoint": 54.54,
"humidity": 0.91,
"pressure": 1012.54,
"windSpeed": 5.12,
"windGust": 8.86,
"windBearing": 254,
"cloudCover": 0.32,
"uvIndex": 0,
"visibility": 8.66,
"ozone": 312.39
*/
}
