//
//  ForecastWeather.swift
//  AlamofirePractise
//
//  Created by Vahida Sheikh on 21/07/18.
//  Copyright © 2018 Vahida Sheikh. All rights reserved.
//

import Foundation
import Alamofire
class forecastWeather{
    //c
    let forecastAPIKey:String
    let forecastBaseURL:URL?
    init(APIKey:String) {
        self.forecastAPIKey = APIKey
        forecastBaseURL = URL(string:"https://api.darksky.net/forecast/\(APIKey)")
        
    }
    func getCurrentWeather(Latitude:Double,Longitude:Double, completion:@escaping (Weather?) ->Void){
        if let forecastURl = URL(string: "\(forecastBaseURL!)/\(Latitude),\(Longitude)"){
            Alamofire.request(forecastURl).responseJSON { (response) in
                if let jsonDictionary = response.result.value as? [String:Any]{
                    if let currentWeatherDictionary = jsonDictionary["currently"] as? [String:Any]{
                        let currentWeather = Weather(weatherDictionary:currentWeatherDictionary)
                        completion(currentWeather)
                    }else{
                        completion(nil)
                    }
                }
            }
    }
  }
}
